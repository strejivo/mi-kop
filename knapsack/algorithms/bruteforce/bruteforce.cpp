#include "bruteforce.h"

using std::vector;

namespace knapsack {
	bool bruteforce::decide(vector<item>::const_iterator begin,
							vector<item>::const_iterator end,
							weight_t capacity, price_t min_price, weight_t weight,
							price_t price) const {
		const item& item = *begin++;
		bool last_item = (begin == end);
		weight_t new_weight = weight + item.weight;
		price_t new_price = price + item.price;
		if (new_weight <= capacity && new_price >= min_price) {
			return true;
		} else if (!last_item) {
			return decide(begin, end, capacity, min_price, new_weight, new_price) ||
				   decide(begin, end, capacity, min_price, weight, price);
		}
		return false;
	}

	bool bruteforce::decide(const decide_problem& problem) const {
		const auto& items = problem.items;
		if (problem.min_price == 0) {
			return true;
		}
		if (items.empty()) {
			return problem.min_price == 0;
		}
		return decide(items.cbegin(), items.cend(), problem.bag_capacity, problem.min_price, 0, 0);
	}

	void bruteforce::construct(std::vector<item>::const_iterator begin, std::vector<item>::const_iterator end,
							   weight_t capacity, weight_t weight, price_t price, const std::vector<bool>& included,
							   std::vector<bool>::iterator included_it, solution_t& best_solution) const {
		const item& current_item = *begin++;
		auto next_included_it = std::next(included_it);
		weight_t new_weight = weight + current_item.weight;
		price_t new_price = price + current_item.price;
		bool last_item = (begin == end);
		if (!last_item) {
			construct(begin, end, capacity, weight, price, included, next_included_it, best_solution);
		}
		*included_it = true;
		if (new_weight <= capacity && (new_price > best_solution.price ||
									   (new_price == best_solution.price && new_weight < best_solution.weight))) {
			best_solution.included = included;
			best_solution.weight = new_weight;
			best_solution.price = new_price;
		}
		if (!last_item) {
			construct(begin, end, capacity, new_weight, new_price, included,
					  next_included_it, best_solution);
		}
		*included_it = false;
	}

	solution_t bruteforce::construct(const construct_problem& problem) const {
		const auto& items = problem.items;
		solution_t best_solution(items.size());
		if (items.empty()) {
			return best_solution;
		}
		std::vector<bool> included(items.size(), false);
		construct(items.cbegin(), items.cend(), problem.bag_capacity, 0, 0, included, included.begin(), best_solution);
		return best_solution;
	}

	std::shared_ptr<const bruteforce> bruteforce::instance() {
		static std::shared_ptr<bruteforce> instance(new bruteforce);
		return instance;
	}
}
