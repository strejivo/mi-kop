#ifndef MI_KOP_KNAPSACK_BRUTEFORCE_H
#define MI_KOP_KNAPSACK_BRUTEFORCE_H

#include <memory>
#include "../../knapsack_common.h"

namespace knapsack {
	class bruteforce : public decider, public constructor {
	  public:
		static std::shared_ptr<const bruteforce> instance();

		bruteforce(const bruteforce&) = delete;

		bruteforce& operator=(const bruteforce&) = delete;

		solution_t construct(const construct_problem& problem) const final;

		bool decide(const decide_problem& problem) const final;

	  private:

		bruteforce() = default;

		bool decide(std::vector<item>::const_iterator begin, std::vector<item>::const_iterator end, weight_t capacity,
					price_t min_price, weight_t weight, price_t price) const;

		void construct(std::vector<item>::const_iterator begin, std::vector<item>::const_iterator end,
					   weight_t capacity,
					   weight_t weight, price_t price, const std::vector<bool>& included,
					   std::vector<bool>::iterator included_it, solution_t& best_solution) const;

	};
}

#endif //MI_KOP_KNAPSACK_BRUTEFORCE_H
