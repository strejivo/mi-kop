#include <random>
#include "simulated_cooling.h"
#include "algorithm"

namespace knapsack {

	solution_t simulated_cooling::construct(const construct_problem& problem) const {
		solution_t best_sol(problem.items.size());
		for (size_t i = 0; i < 25; ++i) {
			solution_t sol = simulate_cooling(problem.bag_capacity, problem.items,
											  m_starting_temp, m_minimum_temp, m_cool_coef, 100);
			if (sol > best_sol) {
				best_sol = sol;
			}
		}
		return best_sol;
	}

	void simulated_cooling::throw_out_overweight(o_solution_t& sol, weight_t max_weight,
												 const std::vector<item>& items) {
		static std::random_device rd;
		static std::default_random_engine gen(rd());
		while (sol.sol.weight > max_weight) {
			std::uniform_int_distribution<size_t> dist(0, sol.included.size() - 1);
			size_t index_to_remove = dist(gen);
			size_t item_index = sol.included[index_to_remove];
			sol.included.erase(sol.included.begin() + index_to_remove);
			sol.excluded.push_back(item_index);
			sol.sol.included[item_index] = false;
			sol.sol.price -= items[item_index].price;
			sol.sol.weight -= items[item_index].weight;
		}
	}

	simulated_cooling::o_solution_t simulated_cooling::create_random_initial_solution(weight_t max_weight,
																 const std::vector<item>& items) {
		static std::random_device rd;
		static std::default_random_engine gen(rd());
		std::uniform_int_distribution<size_t> dist(0, 1);
		solution_t sol(items.size());
		std::vector<size_t> included, excluded;
		for (size_t i = 0; i < items.size(); ++i) {
			if (dist(gen) == 1) {
				sol.included[i] = true;
				sol.price += items[i].price;
				sol.weight += items[i].weight;
				included.push_back(i);
			} else {
				excluded.push_back(i);
			}
		}
		auto o_sol = o_solution_t{sol, included, excluded};
		if (sol.weight > max_weight) {
			throw_out_overweight(o_sol, max_weight, items);
		}
		return o_sol;
	}

	simulated_cooling::o_solution_t
	simulated_cooling::create_random_iteration(const o_solution_t& sol, weight_t max_weight,
											   const std::vector<item>& items) {
		static std::random_device rd;
		static std::default_random_engine gen(rd());
		o_solution_t new_sol(sol);
		if (!new_sol.excluded.empty()) {
			std::uniform_int_distribution<size_t> dist(0, new_sol.excluded.size() - 1);
			size_t added_index = dist(gen);
			size_t item_index = new_sol.excluded[added_index];
			new_sol.excluded.erase(new_sol.excluded.begin() + added_index);
			new_sol.included.push_back(item_index);
			new_sol.sol.price += items[item_index].price;
			new_sol.sol.included[item_index] = true;
			new_sol.sol.weight += items[item_index].weight;
		}
		if (new_sol.sol.weight > max_weight) {
			throw_out_overweight(new_sol, max_weight, items);
		}
		return new_sol;
	}

	solution_t
	simulated_cooling::simulate_cooling(weight_t max_weight, const std::vector<item>& items,
										temperature_t starting_temp,
										temperature_t min_temp,
										temperature_t cool_coef,
										size_t inner_loop_cnt) {
		o_solution_t current_iteration = create_random_initial_solution(max_weight, items);
		o_solution_t best_sol = current_iteration;
		for (temperature_t temp = starting_temp; temp > min_temp; temp *= cool_coef) {
			for (size_t i = 0; i < inner_loop_cnt; ++i) {
				o_solution_t new_iteration = create_random_iteration(current_iteration, max_weight, items);
				if (new_iteration.sol > current_iteration.sol) {
					if (new_iteration.sol > best_sol.sol) {
						best_sol = new_iteration;
					}
					current_iteration = new_iteration;
				} else if (accept(current_iteration.sol.price - new_iteration.sol.price, temp)) {
					current_iteration = new_iteration;
				}
			}
		}
		return best_sol.sol;
	}

	bool simulated_cooling::accept(price_t price_diff, simulated_cooling::temperature_t temp) {
		static std::random_device rd;
		static std::default_random_engine gen(rd());
		static std::uniform_real_distribution<double> dist(0, 1);

		return std::exp(-price_diff / temp) >= dist(gen);
	}

	simulated_cooling::simulated_cooling(temperature_t starting_temp, temperature_t minimum_temp,
										 temperature_t cool_coef)
	  : m_starting_temp(starting_temp),
		m_minimum_temp(minimum_temp),
		m_cool_coef(cool_coef) {}
}
