#ifndef MI_KOP_SIMULATED_COOLING_H
#define MI_KOP_SIMULATED_COOLING_H


#include "../../knapsack_common.h"

namespace knapsack {
	class simulated_cooling : public knapsack::constructor {
	  public:

		using temperature_t = double;
		using temperature_diff_t = double;

		simulated_cooling(temperature_t starting_temp, temperature_t minimum_temp, temperature_t cool_coef);

		solution_t construct(const construct_problem& problem) const override;

	  private:

		struct o_solution_t {
			solution_t sol;
			std::vector<size_t> included;
			std::vector<size_t> excluded;
		};

		static solution_t simulate_cooling(weight_t max_weight, const std::vector<item>& items,
										   temperature_t starting_temp, temperature_t min_temp,
										   temperature_diff_t temp_diff, size_t inner_loop_cnt);

		static o_solution_t create_random_initial_solution(weight_t max_weight, const std::vector<item>& items);

		static o_solution_t
		create_random_iteration(const o_solution_t& sol, weight_t max_weight, const std::vector<item>& items);

		static void throw_out_overweight(o_solution_t& sol, weight_t max_weight, const std::vector<item>& items);

		static bool accept(price_t price_diff, temperature_t temp);

		temperature_t m_starting_temp;
		temperature_t m_minimum_temp;
		temperature_t m_cool_coef;
	};
}


#endif //MI_KOP_SIMULATED_COOLING_H
