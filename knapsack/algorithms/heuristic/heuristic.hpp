#ifndef MI_KOP_HEURISTIC_HPP
#define MI_KOP_HEURISTIC_HPP

#include "../../knapsack_common.h"
#include <functional>

namespace knapsack {
	template<typename T>
	class heuristic : public decider, public constructor {
	  public:
		heuristic(std::function<T(const item&)> heuristic, bool stop_at_fail = false);

		solution_t construct(const construct_problem& problem) const final;

		bool decide(const decide_problem& problem) const final;

	  private:

		struct valued_item {
			valued_item(const T& value, const std::vector<item>::const_iterator& item_it) : value(value),
																							item_it(item_it) {}

			T value;
			std::vector<item>::const_iterator item_it;
		};

		std::vector<valued_item> value_sort_items(const std::vector<item>& items) const {
			std::vector<valued_item> valued_items;
			{
				auto helper_it = items.begin();
				std::for_each(items.begin(), items.end(), [this, &valued_items, &helper_it](const item& item) {
					valued_items.emplace_back(m_heuristic(item), helper_it++);
				});
			}
			std::sort(valued_items.begin(), valued_items.end(),
					  [](const valued_item& i1, const valued_item& i2) {
						  return i1.value > i2.value;
					  });
			return valued_items;
		}

		const std::function<T(const item&)> m_heuristic;
		const bool _stop_at_fail;
	};


	template<typename T>
	heuristic<T>::heuristic(std::function<T(const item&)> heuristic, bool stop_at_fail)
			: m_heuristic(heuristic), _stop_at_fail(stop_at_fail) {}

	template<typename T>
	bool
	heuristic<T>::decide(const decide_problem& problem) const {
		const auto& items = problem.items;
		auto valued_items = value_sort_items(items);
		price_t price = 0;
		weight_t weight = 0;
		for (const auto& i : valued_items) {
			const item& current_item = *i.item_it;
			weight_t new_weight = weight + current_item.weight;
			if (new_weight <= problem.bag_capacity) {
				price += current_item.price;
				if (price >= problem.min_price)
					return true;
				weight = new_weight;
			} else if (_stop_at_fail) {
				break;
			}
		}
		return false;
	}

	template<typename T>
	solution_t
	heuristic<T>::construct(const construct_problem& problem) const {
		const auto& items = problem.items;
		solution_t solution(items.size());
		auto valued_items = value_sort_items(items);
		price_t& price = solution.price;
		weight_t& weight = solution.weight;
		for (const auto& i : valued_items) {
			size_t index = std::distance(items.begin(), i.item_it);
			const item& current_item = *i.item_it;
			weight_t new_weight = weight + current_item.weight;
			if (new_weight <= problem.bag_capacity) {
				price += current_item.price;
				weight = new_weight;
				solution.included[index] = true;
			} else if (_stop_at_fail) {
				break;
			}
		}
		return solution;
	}

}

#endif //MI_KOP_HEURISTIC_HPP
