#include "greedy_heuristic.h"

namespace knapsack {
	greedy_heuristic::greedy_heuristic()
			: heuristic([](const item& item) -> double { return (double) item.price / item.weight; }, false) {}

	std::shared_ptr<const greedy_heuristic> greedy_heuristic::instance() {
		static std::shared_ptr<greedy_heuristic> instance(new greedy_heuristic());
		return instance;
	}
}