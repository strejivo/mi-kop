#ifndef MI_KOP_GREEDY_HEURISTIC_H
#define MI_KOP_GREEDY_HEURISTIC_H


#include "heuristic.hpp"
#include <memory>

namespace knapsack {
	class greedy_heuristic : public heuristic<double> {
	  public:
		static std::shared_ptr<const greedy_heuristic> instance();

	  private:
		greedy_heuristic();
	};
}

#endif //MI_KOP_GREEDY_HEURISTIC_H
