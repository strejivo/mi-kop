#ifndef MI_KOP_REDUX_HPP
#define MI_KOP_REDUX_HPP


#include "../../knapsack_common.h"
#include <memory>
#include <algorithm>

namespace knapsack {
	class redux : public constructor, public decider {
	  public:
		static std::shared_ptr<const redux> instance();

		bool decide(const decide_problem& problem) const override;

		solution_t construct(const construct_problem& problem) const override;

	  private:
		redux() = default;
	};

	std::shared_ptr<const redux> redux::instance() {
		static std::shared_ptr<redux> instance(new redux());
		return instance;
	}

	bool redux::decide(const decide_problem& problem) const {
		weight_t bag_capacity = problem.bag_capacity;
		price_t min_price = problem.min_price;
		for (const item& item : problem.items) {
			if (item.weight <= bag_capacity) {
				if (item.price >= min_price) {
					return true;
				}
			}
		}
		return false;
	}

	solution_t redux::construct(const construct_problem& problem) const {
		solution_t solution(problem.items.size());
		size_t max_item_index = 0;
		size_t max_item_price = 0;
		size_t max_item_weight = 0;
		bool any_fits = false;
		weight_t bag_capacity = problem.bag_capacity;
		for (size_t i = 0; i < problem.items.size(); ++i) {
			const item& item = problem.items[i];
			if (item.weight <= bag_capacity) {
				any_fits = true;
				if (item.price > max_item_price || (item.price == max_item_price && item.weight < max_item_weight)) {
					max_item_index = i;
					max_item_price = item.price;
					max_item_weight = item.weight;
				}
			}
		}
		if (any_fits) {
			solution.included[max_item_index] = true;
			solution.weight = max_item_weight;
			solution.price = max_item_price;
		}
		return solution;
	}
}

#endif //MI_KOP_REDUX_HPP
