#include "fptas.h"
#include "dynamic_price.h"
#include <functional>
#include <cmath>

namespace knapsack {

	solution_t fptas::construct(const construct_problem& problem) const {
		const auto& items = problem.items;
		price_t max_price = std::max_element(items.begin(), items.end(),
											 [](const item& i1, const item& i2) {
												 return i1.price < i2.price;
											 }
		)->price;

		double K = m_epsilon * max_price / items.size();
		std::vector<item> mod_items;
		mod_items.reserve(items.size());
		for (const item& i : items) {
			price_t new_price = floor(i.price / K);
			mod_items.emplace_back(i.weight, new_price != 0 ? new_price : 1);
		}
		auto d_price_instance = dynamic_price::instance();
		auto res = d_price_instance->construct(construct_problem(problem.bag_capacity, mod_items));
		price_t actual_sum = 0;
		for (size_t i = 0; i < res.included.size(); ++i) {
			if (res.included[i]) {
				actual_sum += items[i].price;
			}
		}
		res.price = actual_sum;
		return res;
	}
}
