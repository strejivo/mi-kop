#include "dynamic_price.h"
#include <iostream>
#include <numeric>
#include <set>

namespace knapsack {

	std::shared_ptr<const dynamic_price> dynamic_price::instance() {
		static std::shared_ptr<dynamic_price> instance(new dynamic_price());
		return instance;
	}

	solution_t dynamic_price::construct(const construct_problem& problem) const {

		const auto& items = problem.items;
		price_t price_sum = 0;
		weight_t bag_capacity = problem.bag_capacity;
		for (const item& item : items) {
			price_sum += item.price;
		}

		auto solutions(std::make_unique<std::unique_ptr<std::optional<solution_t>[]>[]>(items.size() + 1));
		solutions[0] = std::make_unique<std::optional<solution_t>[]>(price_sum + 1);
		solutions[0][0] = solution_t(0);
		for (size_t i = 1; i <= items.size(); ++i) {
			const item& current_item = items[i - 1];
			solutions[i] = std::make_unique<std::optional<solution_t>[]>(price_sum + 1);
			solutions[i][0] = solution_t(i);
			for (price_t j = 1; j < current_item.price; ++j) {
				auto& sol_no_add = solutions[i - 1][j];
				if (sol_no_add) {
					solutions[i][j] = create_no_add_solution(*sol_no_add);
				}
			}
			for (price_t j = current_item.price; j <= price_sum; ++j) {
				auto& sol_no_add = solutions[i - 1][j];
				auto& sol_add = solutions[i - 1][j - current_item.price];
				if (!sol_add) {
					if (sol_no_add) {
						solutions[i][j] = create_no_add_solution(*sol_no_add);
					}
					continue;
				}
				if (!sol_no_add) {
					solutions[i][j] = create_add_solution(*sol_add, current_item, bag_capacity);
					continue;
				}
				if (sol_no_add->weight > sol_add->weight + current_item.weight) {
					solutions[i][j] = create_add_solution(*sol_add, current_item, bag_capacity);
				} else {
					solutions[i][j] = create_no_add_solution(*sol_no_add);
				}
			}
		}
		for (price_t j = price_sum; j > 0; j--) {
			if (solutions[items.size()][j]) {
				return *solutions[items.size()][j];
			}
		}
		return solution_t(items.size());
	}

	solution_t dynamic_price::create_no_add_solution(const solution_t& sub_solution) {
		solution_t solution(sub_solution);
		solution.included.push_back(false);
		return solution;
	}

	std::optional<solution_t>
	dynamic_price::create_add_solution(const solution_t& sub_solution, const item& item,
									   weight_t bag_capacity) {
		if (sub_solution.weight + item.weight > bag_capacity) {
			return std::nullopt;
		}
		solution_t solution(sub_solution);
		solution.weight += item.weight;
		solution.price += item.price;
		solution.included.push_back(true);
		return solution;
	}
}
