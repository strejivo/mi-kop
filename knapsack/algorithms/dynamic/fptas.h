#ifndef FPTAS_H
#define FPTAS_H


#include "../../knapsack_common.h"
#include "dynamic_price.h"

namespace knapsack {
	class fptas : public constructor {
	  public:
		fptas(double epsilon) : m_epsilon(epsilon) {}

		solution_t construct(const construct_problem& problem) const final;

	  private:
		const double m_epsilon;
	};
}

#endif //FPTAS_H
