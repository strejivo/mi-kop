#include "dynamic_capacity.h"
#include <algorithm>

namespace knapsack {
	solution_t dynamic_capacity::construct(const construct_problem& problem) const {
		const auto& items = problem.items;
		saved_construct_results solutions(new std::shared_ptr<std::optional<solution_t>[]>[items.size() + 1]);
		for (size_t i = 0; i <= items.size(); ++i) {
			solutions[i] = std::shared_ptr<std::optional<solution_t>[]>(new std::optional<solution_t>[problem.bag_capacity + 1]);
		}
		construct_subproblem subproblem{problem.items.crbegin(), problem.items.crend(), problem.bag_capacity};
		return construct(subproblem, solutions);
	}

	const solution_t& dynamic_capacity::construct(const dynamic_capacity::construct_subproblem& subproblem,
												  dynamic_capacity::saved_construct_results& saved_results) {
		static solution_t trivial_solution(0);
		if (subproblem.items_begin == subproblem.items_end) {
			return trivial_solution;
		}
		{
			if (auto res = try_load_result(subproblem, saved_results); res) {
				return res.value();
			}
		}
		const item& current_item = *subproblem.items_begin;
		construct_subproblem new_subproblem(subproblem);
		++new_subproblem.items_begin;
		// Try without adding
		const solution_t& res_no_add = construct(new_subproblem, saved_results);
		// Test if there is even any decision to be made
		if (subproblem.bag_capacity >= current_item.weight) {

			// Try with adding
			new_subproblem.bag_capacity -= current_item.weight;
			const solution_t& res_add = construct(new_subproblem, saved_results);
			price_t add_price = res_add.price + current_item.price;
			weight_t add_weight = res_add.weight + current_item.weight;
			if (is_better_solution(add_price, add_weight, res_no_add.price, res_no_add.weight)) {
				solution_t result(res_add.included, res_add.weight + current_item.weight,
								  res_add.price + current_item.price);
				result.included.push_back(true);
				return save_construct_result(subproblem, result, saved_results);
			}
		}
		solution_t result(res_no_add.included, res_no_add.weight, res_no_add.price);
		result.included.push_back(false);
		return save_construct_result(subproblem, result, saved_results);
	}

	const solution_t&
	dynamic_capacity::save_construct_result(const dynamic_capacity::construct_subproblem& problem, const solution_t& solution,
											saved_construct_results& saved_results) {
		size_t index = std::distance(problem.items_begin, problem.items_end);
		return *(saved_results[index][problem.bag_capacity] = solution);
	}

	std::shared_ptr<const dynamic_capacity> dynamic_capacity::instance() {
		static std::shared_ptr<dynamic_capacity> instance(new dynamic_capacity());
		return instance;
	}

	bool
	dynamic_capacity::is_better_solution(price_t new_price, weight_t new_weight, price_t other_price, weight_t other_weight) {
		return new_price > other_price
			   || (new_price == other_price && new_weight < other_weight);
	}

	std::optional<std::reference_wrapper<const solution_t>>
	dynamic_capacity::try_load_result(const construct_subproblem& problem,
									  const saved_construct_results& saved_results) {
		size_t index = std::distance(problem.items_begin, problem.items_end);
		return saved_results[index][problem.bag_capacity];
	}
}
