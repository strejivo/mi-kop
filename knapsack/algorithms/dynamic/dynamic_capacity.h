#ifndef MI_KOP_DYNAMIC_CAPACITY_H
#define MI_KOP_DYNAMIC_CAPACITY_H

#include "../../knapsack_common.h"
#include <map>
#include <memory>

namespace knapsack {
	class dynamic_capacity : public constructor {
	  public:

		static std::shared_ptr<const dynamic_capacity> instance();

		solution_t construct(const construct_problem& problem) const override;

	  private:
		dynamic_capacity() = default;

		using items_iterator = std::vector<item>::const_reverse_iterator;

		struct construct_subproblem {
			items_iterator items_begin;
			items_iterator items_end;
			weight_t bag_capacity;
		};
		using saved_construct_results = std::shared_ptr<std::shared_ptr<std::optional<solution_t>[]>[]>;

		static std::optional<std::reference_wrapper<const solution_t>>
		try_load_result(const construct_subproblem& problem, const saved_construct_results& saved_results);

		static const solution_t&
		save_construct_result(const construct_subproblem& problem, const solution_t& solution,
							  saved_construct_results& saved_results);

		static const solution_t& construct(const construct_subproblem& subproblem,
										   saved_construct_results& saved_results);

		static bool is_better_solution(price_t new_price, weight_t new_weight, price_t other_price, weight_t other_weight);
	};
}

#endif //MI_KOP_DYNAMIC_CAPACITY_H
