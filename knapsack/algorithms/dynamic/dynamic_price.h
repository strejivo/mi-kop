#ifndef MI_KOP_DYNAMIC_PRICE_H
#define MI_KOP_DYNAMIC_PRICE_H


#include "../../knapsack_common.h"
#include <memory>
#include <map>

namespace knapsack {
	class dynamic_price : public constructor {
	  public:

		static std::shared_ptr<const dynamic_price> instance();

		solution_t construct(const construct_problem& problem) const override;

	  private:

		static solution_t create_no_add_solution(const solution_t& sub_solution);

		static std::optional<solution_t> create_add_solution(const solution_t& sub_solution, const item& item,
															 weight_t bag_capacity);

	};
}


#endif //MI_KOP_DYNAMIC_PRICE_H