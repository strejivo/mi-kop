#ifndef MI_KOP_BNB_H
#define MI_KOP_BNB_H

#include <memory>
#include "../../knapsack_common.h"

namespace knapsack {
	class bnb : public decider, public constructor {
	  public:
		static std::shared_ptr<const bnb> instance();

		bnb(const bnb&) = delete;

		bnb& operator=(const bnb&) = delete;

		solution_t construct(const construct_problem& problem) const final;

		bool decide(const decide_problem& problem) const final;

	  private:

		struct prepared_item {
			prepared_item(const knapsack::item& item, price_t price) : item(item), price(price) {}

			const knapsack::item item;
			const price_t price;
		};


		bnb() = default;

		price_t
		prepare_items(const std::vector<item>& items, std::vector<prepared_item>& prepared_items) const;

		void construct(std::vector<prepared_item>::const_iterator begin,
					   std::vector<prepared_item>::const_iterator end, weight_t capacity,
					   weight_t weight, price_t price, const std::vector<bool>& included,
					   std::vector<bool>::iterator included_it, solution_t& best) const;

		bool decide(std::vector<prepared_item>::const_iterator begin,
					std::vector<prepared_item>::const_iterator end, weight_t capacity,
					price_t min_price, price_t price, price_t weight, price_t& best) const;
	};
}


#endif //MI_KOP_BNB_H
