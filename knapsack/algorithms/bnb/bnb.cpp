#include "bnb.h"
#include <algorithm>

namespace knapsack {
	bool bnb::decide(const decide_problem& problem) const {
		auto& items = problem.items;
		if (problem.min_price == 0) {
			return true;
		}
		if (items.empty()) {
			return problem.min_price == 0;
		}
		std::vector<prepared_item> prepared_items;
		prepared_items.reserve(items.size());
		auto total_sum = prepare_items(items, prepared_items);
		if (total_sum < problem.min_price) {
			return false;
		}
		price_t best = 0;
		return decide(prepared_items.cbegin(), prepared_items.cend(), problem.bag_capacity, problem.min_price, 0, 0, best);
	}

	solution_t bnb::construct(const construct_problem& problem) const {
		auto& items = problem.items;
		solution_t solution(items.size());
		if (items.empty()) {
			return solution;
		}
		std::vector<prepared_item> prepared_items;
		prepared_items.reserve(items.size());
		prepare_items(items, prepared_items);
		std::vector<bool> included(items.size());
		construct(prepared_items.begin(), prepared_items.end(), problem.bag_capacity, 0, 0, included, included.begin(),
				  solution);
		return solution;
	}

	price_t bnb::prepare_items(const std::vector<item>& items,
							   std::vector<prepared_item>& prepared_items) const {
		price_t sum_price = 0;
		std::vector<prepared_item> tmp;
		std::for_each(items.rbegin(), items.rend(), [&sum_price, &tmp](const item& item) -> void {
			tmp.emplace_back(item, sum_price);
			sum_price += item.price;
		});
		std::for_each(tmp.rbegin(), tmp.rend(), [&prepared_items](const prepared_item& item) {
			prepared_items.emplace_back(item.item, item.price);
		});
		return sum_price;
	}

	bool bnb::decide(std::vector<prepared_item>::const_iterator begin,
					 std::vector<prepared_item>::const_iterator end, weight_t capacity,
					 price_t min_price, price_t weight,
					 price_t price, price_t& best) const {
		const prepared_item& current_prep_item = *begin++;
		bool last_item = (begin == end);
		const item& item = current_prep_item.item;
		weight_t new_weight = weight + item.weight;
		if (new_weight <= capacity) {
			price_t new_price = price + item.price;
			if (new_price >= min_price) {
				return true;
			}
			best = std::max(best, new_price);
			if (!last_item) {
				if (decide(begin, end, capacity, min_price, new_weight, new_price, best)) {
					return true;
				}
			}
		}
		price_t possible_best = price + current_prep_item.price;
		if (!last_item) {
			if (possible_best <= best || possible_best < min_price) {
				return false;
			} else {
				return decide(begin, end, capacity, min_price, weight, price, best);
			}
		}
		return false;
	}

	void bnb::construct(std::vector<prepared_item>::const_iterator begin,
						std::vector<prepared_item>::const_iterator end, weight_t capacity,
						weight_t weight, price_t price,
						const std::vector<bool>& included, std::vector<bool>::iterator included_it,
						solution_t& best) const {
		const prepared_item& prep_item = *begin++;
		bool last_item = (begin == end);
		const item& item = prep_item.item;
		auto next_included_it = std::next(included_it);
		// try adding current item
		*included_it = true;
		weight_t new_weight = weight + item.weight;
		if (new_weight <= capacity) {
			price_t new_price = price + item.price;
			if (new_price > best.price ||
				(new_price == best.price && new_weight < best.weight)) {
				best.included = included;
				best.weight = new_weight;
				best.price = new_price;
			}
			if (!last_item) {
				construct(begin, end, capacity, new_weight, new_price, included,
						  next_included_it, best);
			}
		}
		*included_it = false;
		// try without
		if (!last_item) {
			if (price + prep_item.price >= best.price) {
				construct(begin, end, capacity, weight, price, included,
						  next_included_it, best);
			}
		}
	}

	std::shared_ptr<const bnb> bnb::instance() {
		static std::shared_ptr<bnb> instance(new bnb);
		return instance;
	}
}