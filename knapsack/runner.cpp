#include "runner.h"

using std::chrono::time_point;
using std::chrono::duration_cast;
using std::chrono::nanoseconds;
using std::chrono::high_resolution_clock;

std::pair<knapsack::runner::duration, knapsack::solution_t>
knapsack::runner::run_construct(const knapsack::constructor& solver, const knapsack::construct_problem& problem) {
	time_point begin = high_resolution_clock::now();
	auto result = solver.construct(problem);
	time_point end = high_resolution_clock::now();
	duration dur = end - begin;
	return std::make_pair(dur, result);
}

std::vector<std::pair<knapsack::runner::duration, knapsack::solution_t>>
knapsack::runner::run_construct(const knapsack::constructor& solver, const std::vector<construct_problem>& problems) {
	std::vector<std::pair<duration, solution_t>> results;
	results.reserve(problems.size());
for (const auto& problem : problems) {
		results.push_back(run_construct(solver, problem));
	}
	return results;
}
