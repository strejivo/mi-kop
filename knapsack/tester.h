#ifndef MI_KOP_TESTER_H
#define MI_KOP_TESTER_H

#include <map>
#include <ostream>
#include "knapsack_common.h"

namespace knapsack {
	class tester {
	  public:
		tester(std::map<task_id_t, std::vector<optimal_solution_t>> solutions);

		bool test_exact_construct(task_id_t taskId, const constructor& solver, const construct_problem& problem, std::ostream& os);

		double test_non_exact_construct(task_id_t taskId, const constructor& solver, const construct_problem& problem, std::ostream& os);

		std::vector<std::pair<task_id_t, bool>>
		test_exact_construct(const constructor& solver, const std::vector<std::pair<task_id_t, construct_problem>>& problems, std::ostream& os);

		std::vector<std::pair<task_id_t, double>>
		test_non_exact_construct(const constructor& solver, const std::vector<std::pair<task_id_t, construct_problem>>& problems, std::ostream& os);

		bool test_exact_decide(task_id_t taskId, const decider& solver, const decide_problem& problem, std::ostream& os);

		std::vector<std::pair<task_id_t, bool>>
		test_exact_decide(const decider& solver,
						  const std::vector<std::pair<task_id_t, decide_problem>>& problems, std::ostream& os);

	  private:
		const std::map<task_id_t, std::vector<optimal_solution_t>> m_solutions;
	};
}

#endif //MI_KOP_TESTER_H
