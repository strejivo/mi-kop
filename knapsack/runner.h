#ifndef MI_KOP_RUNNER_H
#define MI_KOP_RUNNER_H

#include <map>
#include <ostream>
#include <chrono>
#include "knapsack_common.h"

namespace knapsack {
	class runner {
	  public:
		runner() = delete;

		using duration = std::chrono::nanoseconds;

		static std::pair<duration, solution_t>
		run_construct(const constructor& solver, const construct_problem& problem);

		static std::vector<std::pair<duration, solution_t>>
		run_construct(const constructor& solver, const std::vector<construct_problem>& problems);
	};
}

#endif //MI_KOP_RUNNER_H