#include "tester.h"

#include <utility>
#include <cassert>
#include <iostream>
#include <chrono>

using std::vector;
using std::pair;
using std::move;
using std::map;
using std::vector;
using std::cout;
using std::endl;
using std::chrono::high_resolution_clock;
using std::chrono::time_point;
using std::chrono::duration_cast;
using std::chrono::nanoseconds;

namespace knapsack {
	tester::tester(map<task_id_t, vector<optimal_solution_t>> solutions) : m_solutions(move(solutions)) {}

	bool tester::test_exact_construct(task_id_t taskId, const constructor& solver,
									  const construct_problem& problem, std::ostream& os) {
		auto expected_result_it = m_solutions.find(taskId);
		assert(expected_result_it != m_solutions.end());
		const auto& expected_results = expected_result_it->second;
		time_point begin = high_resolution_clock::now();
		auto result = solver.construct(problem);
		time_point end = high_resolution_clock::now();
		os << duration_cast<nanoseconds>(end - begin).count() << '\n';
		for (const optimal_solution_t& sol : expected_results) {
			if (result == sol) {
				return true;
			}
		}
		return false;
	}

	double tester::test_non_exact_construct(task_id_t taskId, const constructor& solver,
											const construct_problem& problem, std::ostream& os) {
		auto expected_result_it = m_solutions.find(taskId);
		assert(expected_result_it != m_solutions.end());
		const auto& expected_results = expected_result_it->second;
		price_t expected_price = expected_results.front().price;
		time_point begin = high_resolution_clock::now();
		auto result = solver.construct(problem);
		time_point end = high_resolution_clock::now();
		os << duration_cast<nanoseconds>(end - begin).count() << '\n';
		return (expected_price != 0 ? (double) result.price / expected_price : 1);
	}

	bool tester::test_exact_decide(task_id_t taskId, const decider& solver,
								   const decide_problem& problem, std::ostream& os) {
		auto expected_result_it = m_solutions.find(taskId);
		assert(expected_result_it != m_solutions.end());
		bool expected_result = expected_result_it->second.front().price >= problem.min_price;
		time_point begin = high_resolution_clock::now();
		auto result = solver.decide(problem);
		time_point end = high_resolution_clock::now();
		os << duration_cast<nanoseconds>(end - begin).count() << '\n';
		return result == expected_result;
	}

	vector<pair<task_id_t, bool>>
	tester::test_exact_construct(const constructor& solver, const vector<pair<task_id_t, construct_problem>>& problems, std::ostream& os) {
		vector<pair<task_id_t, bool>> test_results;
		test_results.reserve(problems.size());
		for (const auto& problem : problems) {
			auto task_id = problem.first;
			bool test_result = test_exact_construct(task_id, solver, problem.second, os);
			test_results.emplace_back(task_id, test_result);
		}
		return test_results;
	}

	vector<pair<task_id_t, bool>> tester::test_exact_decide(const decider& solver,
															const vector<pair<task_id_t, decide_problem>>& problems, std::ostream& os) {
		vector<pair<task_id_t, bool>> test_results;
		test_results.reserve(problems.size());
		for (const auto& problem : problems) {
			auto task_id = problem.first;
			bool test_result = test_exact_decide(task_id, solver, problem.second, os);
			test_results.emplace_back(task_id, test_result);
		}
		return test_results;
	}

	std::vector<std::pair<task_id_t, double>>
	tester::test_non_exact_construct(const constructor& solver, const std::vector<std::pair<task_id_t, construct_problem>>& problems,
									 std::ostream& os) {
		vector<pair<task_id_t, double>> test_results;
		test_results.reserve(problems.size());
		for (const auto& problem : problems) {
			auto task_id = problem.first;
			auto test_result = test_non_exact_construct(task_id, solver, problem.second, os);
			test_results.emplace_back(task_id, test_result);
		}
		return test_results;
	}
}