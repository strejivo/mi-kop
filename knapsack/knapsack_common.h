#ifndef MI_KOP_KNAPSACK_COMMON_H
#define MI_KOP_KNAPSACK_COMMON_H

#include <utility>
#include <vector>

namespace knapsack {

	using weight_t = unsigned long;
	using price_t = unsigned long;
	using task_id_t = int;

	struct item {
		item(const weight_t weight, const price_t price) : weight(weight), price(price) {}

		item(const item& other) : weight(other.weight), price(other.price) {}

		const weight_t weight;
		const price_t price;
	};

	struct optimal_solution_t {
		optimal_solution_t() = default;

		optimal_solution_t(std::vector<bool> included, price_t price) : included(
				std::move(included)), price(price) {}

		std::vector<bool> included;
		price_t price = 0;
	};

	struct solution_t {
		solution_t(size_t size) : included(size, false), weight(0), price(0) {}

		solution_t(std::vector<bool> included, weight_t weight, price_t price) : included(std::move(included)),
																				 weight(weight), price(price) {}

		bool operator==(const optimal_solution_t& solution) const {
			return price == solution.price && included == solution.included;
		}

		bool operator>(const solution_t& o) const {
			return price > o.price || (price == o.price && weight < o.weight);
		}

		std::vector<bool> included;
		weight_t weight;
		price_t price;
	};

	struct decide_problem {
		decide_problem(const weight_t bagCapacity, const price_t minPrice, std::vector<item> items)
				: bag_capacity(bagCapacity), min_price(minPrice), items(std::move(items)) {}

		const weight_t bag_capacity = 0;
		const price_t min_price = 0;
		const std::vector<item> items;
	};

	struct construct_problem {
		construct_problem(const weight_t bagCapacity, std::vector<item> items) : bag_capacity(bagCapacity),
																				 items(std::move(items)) {}

		construct_problem(const decide_problem& dec_problem)
				: bag_capacity(dec_problem.bag_capacity), items(dec_problem.items) {}

		const weight_t bag_capacity = 0;
		const std::vector<item> items;
	};

	class decider {
	  public:
		virtual ~decider() = default;

		virtual bool decide(const decide_problem& problem) const = 0;

	  protected:
		decider() = default;
	};

	class constructor {
	  public:
		virtual ~constructor() = default;

		virtual solution_t construct(const construct_problem& problem) const = 0;

	  protected:
		constructor() = default;
	};

}
#endif //MI_KOP_KNAPSACK_COMMON_H
