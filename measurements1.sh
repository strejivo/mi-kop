#!/bin/bash

# Loop over all solver algithms
for solver in bnb bruteforce; do
  # Loop over all instance sizes
  for i in 4 10 15 20 22 25 27 30 32 35 37 40; do
    # Loop over all data sets
    for z in N Z; do
      # Create process
      ./cmake-build-release/mi_kop $solver data/input/"$z"R"$i"_inst.dat data/reference/"$z"K"$i"_sol.dat >measurements/"$solver"_"$z"_"$i" &
    done
  done
done
# Now lets wait for all of them to finish
wait
