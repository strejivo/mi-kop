#include <iostream>
#include <filesystem>
#include "sat/tester.h"
#include "sat/mwcnf_format_parser.h"


int main(int argc, char* argv[]) {
	if (argc < 3) {
		std::cerr << "Wrong number of arguments" << std::endl;
		return 1;
	}
	std::vector<std::filesystem::directory_entry> files;
	std::string folder_path = argv[1];
	std::string solution_file_path = argv[2];
	for (const auto& file : std::filesystem::directory_iterator(folder_path)) {
		files.push_back(file);
	}
	std::sort(files.begin(), files.end(), [](const auto& f1, const auto& f2) {
		return f1.path().filename() < f2.path().filename();
	});
	std::vector<std::pair<std::string, sat::weighted_problem>> problems;
	for (const auto& file : files) {
		auto problem = sat::mwcnf_format_parser::parse_input_file(file.path());
		std::string name = file.path().filename().replace_extension();
		problems.emplace_back(name.substr(1, name.find("-A") - 1), problem);
	}
	auto reference_solutions = sat::mwcnf_format_parser::parse_solutions(solution_file_path);
	sat::simulated_cooling::temperature_t starting_temperature = 100;
	sat::simulated_cooling::temperature_t min_temperature = 10;
	sat::simulated_cooling::temperature_diff_coef_t coef = 0.9;
	size_t inner_circle_cnt = 25;
	if (argc >= 4) {
		std::istringstream ss(argv[3]);
		ss >> starting_temperature;
		if (argc >= 5) {
			std::istringstream ss(argv[4]);
			ss >> min_temperature;
			if (argc >= 6) {
				std::istringstream ss(argv[5]);
				ss >> coef;
				if (argc >= 7) {
					std::istringstream ss(argv[6]);
					ss >> inner_circle_cnt;
				}
			}
		}
	}
	sat::test_simulated_cooling(problems, reference_solutions, sat::simulated_cooling(starting_temperature, min_temperature, coef, inner_circle_cnt));
	return 0;
}