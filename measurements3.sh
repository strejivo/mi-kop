#!/bin/bash

for file in data/myinstances/*; do
  for solver in bnb price capacity greedy; do
    BASENAME="$(basename -- $file)"
    echo "$BASENAME" "$solver"
    if [ $solver != greedy ];
      then ./cmake-build-release/mi_kop "$solver" "$file" --ns >measurements/myinstances/"$solver"_"$BASENAME"
      else ./cmake-build-release/mi_kop "$solver" "$file" --ns >measurements/myinstances/"$solver"_"$BASENAME" 2>measurements/myinstances/"$solver"_"$BASENAME"_prec
    fi
  done
done
