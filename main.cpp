#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <set>
#include <memory>
#include <cstring>

#include "knapsack/knapsack_common.h"
#include "knapsack/tester.h"
#include "knapsack/runner.h"
#include "knapsack/algorithms/bruteforce/bruteforce.h"
#include "knapsack/algorithms/bnb/bnb.h"
#include "knapsack/algorithms/heuristic/greedy_heuristic.h"
#include "knapsack/algorithms/heuristic/redux.hpp"
#include "knapsack/algorithms/dynamic/dynamic_capacity.h"
#include "knapsack/algorithms/dynamic/dynamic_price.h"
#include "knapsack/algorithms/dynamic/fptas.h"
#include "knapsack/algorithms/sim_cooling/simulated_cooling.h"


using knapsack::item;
using knapsack::weight_t;
using knapsack::price_t;
using knapsack::solution_t;
using knapsack::construct_problem;
using knapsack::decide_problem;
using knapsack::optimal_solution_t;
using knapsack::tester;
using knapsack::runner;
using knapsack::decider;
using knapsack::constructor;
using knapsack::task_id_t;

using std::vector;
using std::map;
using std::set;
using std::string;
using std::shared_ptr;
using std::istream;
using std::ifstream;
using std::pair;
using std::cout;
using std::endl;
using std::cerr;


vector<item> load_items(istream& stream, size_t cnt) {
	vector<item> items;
	weight_t item_weight;
	price_t item_price;
	for (size_t i = 0; i < cnt; ++i) {
		stream >> item_weight >> item_price;
		items.emplace_back(item_weight, item_price);
	}
	return items;
}


decide_problem load_decide_knapsack_problem(ifstream& ifs) {
	size_t items_cnt;
	weight_t capacity;
	price_t min_price;
	ifs >> items_cnt >> capacity >> min_price;
	auto items = load_items(ifs, items_cnt);
	return decide_problem(capacity, min_price, items);
}

construct_problem load_construct_knapsack_problem(ifstream& ifs) {
	weight_t capacity;
	size_t items_cnt = 0;
	ifs >> items_cnt >> capacity;
	auto items = load_items(ifs, items_cnt);
	return construct_problem(capacity, items);
}

optimal_solution_t load_knapsack_solution(ifstream& ifs) {

	optimal_solution_t solution;
	size_t items_cnt;
	ifs >> items_cnt >> solution.price;
	for (size_t i = 0; i < items_cnt; ++i) {
		short included = 0;
		ifs >> included;
		solution.included.emplace_back(included == 1);
	}
	return solution;
}

void process_results(const vector<pair<task_id_t, bool>>& results, std::ostream& os) {
	set<task_id_t> failed;
	for (const auto& result : results) {
		if (!result.second) {
			failed.insert(result.first);
		}
	}
	size_t passed = results.size() - failed.size();
	os << passed << " passed, " << failed.size() << " failed";
	if (!failed.empty()) {
		os << " [ ";
		for (const task_id_t& task_id : failed) {
			os << task_id << " ";
		}
		os << "]";
	}
	os << endl;
}

void process_results(const vector<pair<task_id_t, double>>& results, std::ostream& os) {
	double worst = 1;
	double sum = 0;
	for (const auto&[task_id, precision] : results) {
		if (precision < worst) {
			worst = precision;
		}
		sum += precision;
		cout << precision << '\n';
	}
	os << "average precision: " << sum / results.size() << "\nworst precision: " << worst << '\n';
}

int main(int argc, char* argv[]) {
	if (argc != 4) {
		cerr << "Expected exactly 3 argument - solver type, input file and reference file." << '\n';
		return 1;
	}
	cout.precision(std::numeric_limits<double>::max_digits10);
	string solver_str = argv[1];

	shared_ptr<const decider> decider;
	shared_ptr<const constructor> constructor;
	bool exact = true;
	if (solver_str == "bruteforce") {
		decider = knapsack::bruteforce::instance();
		constructor = knapsack::bruteforce::instance();
	} else if (solver_str == "bnb") {
		decider = knapsack::bnb::instance();
		constructor = knapsack::bnb::instance();
	} else if (solver_str == "capacity") {
		constructor = knapsack::dynamic_capacity::instance();
	} else if (solver_str == "price") {
		constructor = knapsack::dynamic_price::instance();
	} else if (solver_str == "fptas") {
		exact = false;
		constructor = std::make_shared<knapsack::fptas>(0.1);
	} else if (solver_str == "greedy") {
		exact = false;
		constructor = knapsack::greedy_heuristic::instance();
	} else if (solver_str == "redux") {
		exact = false;
		constructor = knapsack::redux::instance();
	} else if (solver_str == "cooling") {
		exact = false;
		constructor = std::make_shared<knapsack::simulated_cooling>(40, 2, 0.975);
	} else {
		cerr << "Unknown solver type" << endl;
		return 1;
	}
	bool only_runtime = false;
	task_id_t task_id;
	if (strcmp(argv[3], "--ns") == 0) {
		only_runtime = true;
	}
	vector<pair<task_id_t, construct_problem>> construct_problems;
	vector<pair<task_id_t, decide_problem>> decide_problems;

	{
		ifstream ifs(argv[2]);
		if (!ifs) {
			cerr << "File " << argv[2] << " with problems can not be opened" << '\n';
			return 1;
		}
		while (ifs >> task_id) {
			if (task_id < 0) {
				task_id *= -1;
				auto problem = load_decide_knapsack_problem(ifs);
				decide_problems.emplace_back(task_id, problem);
			} else {
				auto problem = load_construct_knapsack_problem(ifs);
				construct_problems.emplace_back(task_id, problem);
			}
		}
	}

	if (!only_runtime) {
		map<task_id_t, std::vector<optimal_solution_t>> solutions;
		{
			ifstream sifs(argv[3]);
			if (!sifs) {
				cerr << "File " << argv[3] << " with solutions can not be opened" << '\n';
				return 1;
			}
			while (sifs >> task_id) {
				auto solution = load_knapsack_solution(sifs);
				solutions[task_id].push_back(solution);
			}
		}
		tester tester(solutions);

		if (!construct_problems.empty()) {
			if (!constructor) {
				cerr << "Not valid solver for construct problems" << '\n';
				return 1;
			} else {
				if (exact) {
					auto test_results = tester.test_exact_construct(*constructor, construct_problems, cout);
					process_results(test_results, cerr);
				} else {
					auto test_results = tester.test_non_exact_construct(*constructor, construct_problems, cout);
					process_results(test_results, cerr);
				}
			}
		}
		if (!decide_problems.empty()) {
			if (!decider) {
				cerr << "Not valid solver for decide problems" << '\n';
				return 1;
			} else {
				auto test_results = tester.test_exact_decide(*decider, decide_problems, cout);
				process_results(test_results, cerr);
			}
		}
	} else {
		vector<construct_problem> construct_problems_no_id;
		construct_problems_no_id.reserve(construct_problems.size());
		for (const auto& [task_id, problem] : construct_problems) {
			construct_problems_no_id.push_back(problem);
		}
		auto results = runner::run_construct(*constructor, construct_problems_no_id);
		for (const auto& res : results) {
			std::cout<< res.first.count() << '\n';
		}
		if (!exact) {
			auto exact_results = runner::run_construct(*knapsack::bnb::instance(), construct_problems_no_id);
			long double sum = 0;
			for (size_t i = 0; i < construct_problems_no_id.size(); ++i) {
				std::cerr<<results[i].second.price << " "  << exact_results[i].second.price << '\n';
				if (exact_results[i].second.price != 0) {
					sum += (double)results[i].second.price / exact_results[i].second.price;
				} else {
					sum += 1;
				}
			}
			std::cerr<<sum/construct_problems_no_id.size()<<std::endl;
		}
	}
	return 0;
}
