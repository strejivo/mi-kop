#!/bin/bash

INST_SIZE=1000
OUTPUT_FOLDER=data/myinstances

PROG=./gen/kg2
ITEMS_COUNT=25
MAX_WEIGHT=3000
MAX_PRICE=3000

for price_weight_corr in uni corr strong; do
  "${PROG}" -r time -n $ITEMS_COUNT -N "$INST_SIZE" -W "$MAX_WEIGHT" -w bal -C "$MAX_PRICE" -c "$price_weight_corr" -m 0.8 > "$OUTPUT_FOLDER"/PRICE_"$price_weight_corr"
done

for bag_capacity_ratio in 0.1 0.3 0.5 0.8 1 2; do
  "${PROG}" -r time -n $ITEMS_COUNT -N "$INST_SIZE" -W "$MAX_WEIGHT" -w bal -C "$MAX_PRICE" -c uni -m "$bag_capacity_ratio" > "$OUTPUT_FOLDER"/BAG_RATIO_"$bag_capacity_ratio"
done

for weight_distribution in bal light heavy; do
    if [ "$weight_distribution" != bal ]; then
      for k in 0.1 0.2 0.5 1 3 7; do
        "${PROG}" -r time -n $ITEMS_COUNT -N "$INST_SIZE" -W "$MAX_WEIGHT" -w "$weight_distribution" -C "$MAX_PRICE" -c "uni" -m 0.8 -k "$k" > "$OUTPUT_FOLDER"/WEIGHT_DIST_"$weight_distribution"_"$k"
      done
    else
      "${PROG}" -r time -n $ITEMS_COUNT -N "$INST_SIZE" -W "$MAX_WEIGHT" -w "$weight_distribution" -C "$MAX_PRICE" -c "uni" -m 0.8 > "$OUTPUT_FOLDER"/WEIGHT_DIST_"$weight_distribution"
    fi;
done
