#ifndef WEIGHTED_PROBLEM_H
#define WEIGHTED_PROBLEM_H

#include "cnf.h"

namespace sat {

	using weight_t = unsigned long;

	class weighted_problem {
	  public:
		weighted_problem(cnf_formula formula, std::vector<weight_t> weights) : formula(std::move(formula)),
																			   weights(std::move(weights)) {}

		class reference_solution;

		class solution;

		const cnf_formula formula;
		const std::vector<weight_t> weights;
	};
}


#endif //WEIGHTED_PROBLEM_H
