#include <memory>
#include <iostream>
#include "simulated_cooling.h"
#include "../../shared_random.h"

namespace sat {
	using solution_t = weighted_problem::solution;

	simulated_cooling::simulated_cooling(simulated_cooling::temperature_t initial_temperature,
										 simulated_cooling::temperature_t minimal_temperature,
										 simulated_cooling::temperature_diff_coef_t temperature_diff_coef,
										 size_t inner_loop_cnt)
	  : m_initial_temperature(initial_temperature),
		m_minimal_temperature(minimal_temperature),
		m_temperature_coef(temperature_diff_coef),
		m_inner_loop_cnt(inner_loop_cnt) {}

	std::optional<weighted_problem::solution> simulated_cooling::solve(const weighted_problem& problem) const {
		return solve(problem, m_initial_temperature, m_minimal_temperature, m_temperature_coef, m_inner_loop_cnt);
	}

	std::optional<weighted_problem::solution>
	simulated_cooling::solve(const weighted_problem& problem, simulated_cooling::temperature_t begin_temperature,
							 simulated_cooling::temperature_t minimal_temperature,
							 simulated_cooling::temperature_diff_coef_t temperature_diff_coef,
							 size_t inner_loop_cnt) {
		auto opt_initial_solution = create_initial_solution(problem);
		if (!opt_initial_solution.has_value()) {
			return std::nullopt;
		}
		working_solution current_solution = opt_initial_solution.value();
		solution_t best_solution = current_solution;

		for (temperature_t temperature = begin_temperature;
			 temperature >= minimal_temperature; temperature *= temperature_diff_coef) {
			for (size_t i = 0; i < inner_loop_cnt; ++i) { // TODO change to something more smart
				auto temp = create_next_iteration(current_solution);
				if (!temp.has_value()) {
					continue;
				}
				if (temp.value() > current_solution) {
					current_solution = std::move(temp.value());
					if (current_solution > best_solution) {
						best_solution = current_solution;
					}
				} else if (accept_worse_solution(current_solution, temp.value(), temperature)) {
					current_solution = std::move(temp.value());
				}
				std::cout<<current_solution.weight()<<std::endl;
			}
		}

		return best_solution;
	}

	std::optional<working_solution>
	simulated_cooling::create_initial_solution(const weighted_problem& problem) {
		// Set everything to true
		evaluation_t values(problem.formula.included_variables().size(), true);
		// Calculate new weight
		weight_t weight = 0;
		for (const variable_t& v : problem.formula.included_variables()) {
			weight += problem.weights[v];
		}

		auto initial_solution = working_solution(values, weight, problem);
		// Check if valid solution
		if (initial_solution.satisfies_problem()) {
			return initial_solution;
		} else {
			// Fix it
			return create_next_iteration(initial_solution);
		}
	}

	std::optional<working_solution> simulated_cooling::create_next_iteration(const working_solution& solution) {
		working_solution iteration = solution;

		size_t max_tries = std::pow(solution.problem().formula.included_variables().size(), 3);

		for (size_t i = 0; i < max_tries; ++i) {
			// Do smart flip
			iteration.smart_flip_random_variable();
			if (iteration.satisfies_problem()) {
				return iteration;
			}
		}
		// Valid solution wasn't found in given number of tries
		return std::nullopt;
	}

	bool simulated_cooling::accept_worse_solution(const weighted_problem::solution& current_solution,
												  const weighted_problem::solution& suggested_solution,
												  simulated_cooling::temperature_t temperature) {

		weight_t weight_diff = current_solution.weight() - suggested_solution.weight();

		return std::exp(-weight_diff / temperature) >= acceptance_percentage();
	}
}