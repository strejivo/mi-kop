#include "working_solution.h"
#include "../../cnf.h"
#include "../../shared_random.h"
#include <set>
#include <utility>


namespace sat {
	void working_solution::flip_random_variable() {
		std::uniform_int_distribution<size_t> dist(0, m_problem.formula.included_variables().size() - 1);

		// Select random variable to be flipped
		variable_t changed_variable = m_problem.formula.included_variables()[dist(random_engine())];
		flip_variable(changed_variable);
	}

	void working_solution::flip_variable(variable_t flipped_variable) {
		if (m_values[flipped_variable]) {
			m_weight -= m_problem.weights[flipped_variable];
		} else {
			m_weight += m_problem.weights[flipped_variable];
		}
		m_values[flipped_variable] = !m_values[flipped_variable];
		m_variable_flipped_occurrences[flipped_variable] = m_problem.formula.affected_clauses(flipped_variable).size() - m_variable_flipped_occurrences[flipped_variable];
		for (size_t clause_index : m_problem.formula.affected_clauses(flipped_variable)) {
			const clause& clause = m_problem.formula.clauses[clause_index];
			bool new_evaluation = clause.evaluate(m_values);
			if (m_clauses_satisfied[clause_index] != new_evaluation) {
				if (new_evaluation) {
					m_unsatisfied_clauses_cnt--;
					for (const auto& l : clause.literals) {
						m_variables_in_unsatisfied_clauses.erase(m_variables_in_unsatisfied_clauses.find(l.variable));
					}
				} else {
					m_unsatisfied_clauses_cnt++;
					for (const auto& l : clause.literals) {
						m_variables_in_unsatisfied_clauses.insert(l.variable);
					}
				}
				m_clauses_satisfied[clause_index] = new_evaluation;
			}
		}
	}

	void working_solution::smart_flip_random_variable() {
		if (satisfies_problem()) {
			smart_flip_random_variable_valid();
		} else {
			smart_flip_random_variable_invalid();
		}
	}

	void working_solution::smart_flip_random_variable_valid() {
		//flip_random_variable();
		while (true) {
			std::uniform_int_distribution<size_t> variable_distribution(0, m_problem.formula.included_variables().size() - 1);
			variable_t candidate = *std::next(m_problem.formula.included_variables().begin(), variable_distribution(random_engine()));
			size_t total_variable_count = m_problem.formula.affected_clauses(candidate).size();
			size_t negation_variable_count = m_variable_flipped_occurrences[candidate];
			double negation_ratio = (double) negation_variable_count / total_variable_count;
			if (acceptance_percentage() <= negation_ratio) {
				flip_variable(candidate);
				break;
			}
		}
	}

	void working_solution::smart_flip_random_variable_invalid() {
		std::uniform_int_distribution<size_t> variable_distribution(0, m_variables_in_unsatisfied_clauses.size() - 1);
		variable_t variable = *std::next(m_variables_in_unsatisfied_clauses.begin(), variable_distribution(random_engine()));
		flip_variable(variable);
	}

	working_solution::working_solution(evaluation_t v, weight_t w, const weighted_problem& problem)
			: solution(std::move(v), w, problem),
			  m_clauses_satisfied(problem.formula.clauses.size()),
			  m_variable_flipped_occurrences(problem.formula.included_variables().size()) {
		m_unsatisfied_clauses_cnt = 0;
		for (size_t i = 0; i < m_problem.formula.clauses.size(); ++i) {
			const auto& clause = m_problem.formula.clauses[i];
			bool satisfied = clause.evaluate(m_values);
			m_clauses_satisfied[i] = satisfied;
			if (!satisfied) {
				m_unsatisfied_clauses_cnt++;
			}
			for (const auto& l : clause.literals) {
				if (l.negation == m_values[l.variable]) {
					++m_variable_flipped_occurrences[l.variable];
				}
				if (!satisfied) {
					m_variables_in_unsatisfied_clauses.insert(l.variable);
				}
			}

		}
	}
}