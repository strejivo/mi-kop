#ifndef MI_KOP_WORKING_SOLUTION_H
#define MI_KOP_WORKING_SOLUTION_H


#include "../../solution.h"
#include <set>

namespace sat {
	class working_solution : public weighted_problem::solution {
	  public:
		working_solution(evaluation_t v, weight_t w, const weighted_problem& problem);

		void flip_variable(variable_t flipped_variable);

		void flip_random_variable();

		void smart_flip_random_variable();

		bool satisfies_problem() const {
			return m_unsatisfied_clauses_cnt == 0;
		}

	  private:
		void smart_flip_random_variable_valid();

		void smart_flip_random_variable_invalid();

		std::vector<bool> m_clauses_satisfied;
		size_t m_unsatisfied_clauses_cnt;
		std::multiset<variable_t> m_variables_in_unsatisfied_clauses;
		std::vector<size_t> m_variable_flipped_occurrences;
	};
}


#endif //MI_KOP_WORKING_SOLUTION_H
