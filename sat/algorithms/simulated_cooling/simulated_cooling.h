#ifndef MI_KOP_SIMULATED_COLLING_H
#define MI_KOP_SIMULATED_COLLING_H

#include "../../cnf.h"
#include "../../weighted_problem.h"
#include "../../solution.h"
#include "working_solution.h"

namespace sat {
	class simulated_cooling {
	  public:
		using temperature_t = double;
		using temperature_diff_coef_t = double;

		simulated_cooling(temperature_t starting_temperature, temperature_t minimal_temperature,
						  temperature_diff_coef_t temperature_diff, size_t inner_loop_cnt);

		std::optional<weighted_problem::solution> solve(const weighted_problem& problem) const;

	  private:

		temperature_t m_initial_temperature;
		temperature_t m_minimal_temperature;
		temperature_diff_coef_t m_temperature_coef;
		size_t m_inner_loop_cnt;

		static std::optional<weighted_problem::solution>
		solve(const weighted_problem& problem, temperature_t begin_temperature, temperature_t minimal_temperature,
			  temperature_diff_coef_t temperature_diff_coef, size_t inner_loop_cnt);

		static std::optional<working_solution>
		create_next_iteration(const working_solution& solution);

		static bool accept_worse_solution(const weighted_problem::solution& current_solution,
										  const weighted_problem::solution& suggested_solution,
										  temperature_t temperature);

		static std::optional<working_solution> create_initial_solution(const weighted_problem& problem);
	};
}


#endif //MI_KOP_SIMULATED_COLLING_H
