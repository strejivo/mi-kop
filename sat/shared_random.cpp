#include "shared_random.h"


static std::random_device rd;
static std::default_random_engine gen(rd());
static std::uniform_real_distribution<double> acceptance_distribution(0, 1);


double acceptance_percentage() {
	return acceptance_distribution(gen);
}

std::default_random_engine& random_engine() {
	return gen;
}