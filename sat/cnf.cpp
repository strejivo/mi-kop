#include <set>
#include "cnf.h"

namespace sat {

	bool clause::evaluate(const evaluation_t& evaluation) const {
		for (const literal& l : literals) {
			if (l.evaluate(evaluation)) {
				return true;
			}
		}
		return false;
	}

	bool literal::evaluate(const evaluation_t& evaluation) const {
		return evaluation[variable] ^ negation;
	}

	bool cnf_formula::evaluate(const evaluation_t& evaluation) const {
		for (const auto& c : clauses) {
			if (!c.evaluate(evaluation)) {
				return false;
			}
		}
		return true;
	}

	cnf_formula::cnf_formula(std::vector<clause> cls) : clauses(std::move(cls)),
														m_affected_clauses(clauses.size() * 3) {
		std::set<variable_t> variables;
		size_t c_index = 0;
		for (auto c_it = clauses.begin(); c_it != clauses.end(); ++c_it, ++c_index) {
			for (const literal& l: c_it->literals) {
				if (variables.insert(l.variable).second) {
					m_included_variables.push_back(l.variable);
				}
				m_affected_clauses[l.variable].push_back(c_index);
			}
		}
	}

	const std::vector<variable_t>& cnf_formula::included_variables() const {
		return m_included_variables;
	}

	const std::vector<size_t>& cnf_formula::affected_clauses(variable_t variable) const {
		return m_affected_clauses[variable];
	}

}