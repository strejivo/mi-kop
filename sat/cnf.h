#ifndef CNF_H
#define CNF_H

#include <map>
#include <vector>
#include <optional>
#include <functional>
#include <memory>

namespace sat {
	using variable_t = unsigned long;
	using evaluation_t = std::vector<bool>;

	class literal {
	  public:
		literal(variable_t v, bool n = false) : variable(v), negation(n) {}

		bool evaluate(const evaluation_t& evaluation) const;

		const variable_t variable;
		const bool negation;
	};

	class clause {
	  public:
		clause(std::vector<literal> literals) : literals(std::move(literals)) {}

		// Optional is because if there is variable with both negation and non-negation present, it is tautology
		//std::optional<clause> simplify() const;

		bool evaluate(const evaluation_t& evaluation) const;

		const std::vector<literal> literals;
	};

	class cnf_formula {
	  public:
		cnf_formula(std::vector<clause> clauses);

		bool evaluate(const evaluation_t& evaluation) const;

		const std::vector<clause> clauses;

		const std::vector<variable_t>& included_variables() const;

		const std::vector<size_t>& affected_clauses(variable_t variable) const;

	  private:
		std::vector<variable_t> m_included_variables;

		std::vector<std::vector<size_t>> m_affected_clauses;
	};
}


#endif //CNF_H
