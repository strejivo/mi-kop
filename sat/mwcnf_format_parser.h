#ifndef MI_KOP_MWCNF_FORMAT_PARSER_H
#define MI_KOP_MWCNF_FORMAT_PARSER_H


#include <sstream>
#include <fstream>
#include <cassert>
#include "cnf.h"
#include "weighted_problem.h"
#include "reference_solution.h"

namespace sat::mwcnf_format_parser {

	std::vector<weight_t> parse_weight_line(std::ifstream& ifs, size_t variables_cnt) {
		std::vector<weight_t> weights;
		weights.reserve(variables_cnt);
		weight_t weight = 0;
		for (variable_t var = 0; var < variables_cnt; ++var) {
			if (!(ifs >> weight)) {
				throw std::runtime_error("Bad weight formatting");
			}
			weights.push_back(weight);
		}
		ifs >> weight;
		assert(weight == 0);
		return weights;
	}

	std::vector<literal> parse_literals(std::ifstream& ifs) {
		variable_t var = 1;
		std::vector<literal> literals;
		while (true) {
			ifs >> std::ws;
			bool negation = false;
			if (ifs.peek() == '-') {
				negation = true;
				ifs.ignore();
			}
			ifs >> var;
			if (var == 0) {
				break;
			}
			literals.emplace_back(var - 1, negation);
		}
		return literals;
	}

	clause parse_clause(std::ifstream& ifs) {
		return clause(parse_literals(ifs));
	}

	weighted_problem parse_input_file(const std::string& file_name) {
		std::ifstream ifs(file_name);
		if (!ifs) {
			throw std::runtime_error("Unable to open given file");
		}
		std::vector<weight_t> weights;
		std::vector<clause> clauses;
		size_t variable_cnt = 0;
		size_t clause_cnt = 0;
		while (true) {
			ifs >> std::ws;
			if (ifs.peek() == 'c') {
				// Skip this line
				ifs.ignore(std::numeric_limits<std::streamsize>::max(), ifs.widen('\n'));
				continue;
			}
			if (ifs.peek() == 'p') {
				ifs.ignore();
				ifs >> std::ws;
				std::string s;
				ifs >> s;
				assert(s == "mwcnf");
				ifs >> variable_cnt;
				ifs >> clause_cnt;
				clauses.reserve(clause_cnt);
				continue;
			}
			if (ifs.peek() == 'w') {
				assert(variable_cnt > 0);
				ifs.ignore();
				weights = parse_weight_line(ifs, variable_cnt);
				continue;
			}
			if (ifs.peek() == '%') {
				ifs.ignore();
				continue;
			}
			if (ifs.peek() != '0') {
				assert(variable_cnt > 0);
				assert(clauses.size() < clause_cnt);
				clauses.emplace_back(parse_clause(ifs));
				continue;
			} else {
				break;
			}
		}
		assert(weights.size() == variable_cnt);
		assert(clauses.size() == clause_cnt);
		return weighted_problem(cnf_formula(clauses), weights);
	}

	std::map<std::string, std::vector<weighted_problem::reference_solution>>
	parse_solutions(const std::string& file_name) {
		std::map<std::string, std::vector<weighted_problem::reference_solution>> solutions;
		std::ifstream ifs(file_name);
		while (true) {
			ifs >> std::ws;
			if (ifs.peek() != EOF) {
				std::string problem_id;
				weight_t weight;
				ifs >> problem_id >> weight;
				std::vector<literal> literals = parse_literals(ifs);
				std::vector<bool> values(literals.size());
				for (const auto& literal : literals) {
					values[literal.variable] = !literal.negation;
				}
				solutions[problem_id].push_back(weighted_problem::reference_solution(values, weight));
			} else {
				break;
			}
		}
		return solutions;
	}
}


#endif //MI_KOP_MWCNF_FORMAT_PARSER_H
