#include <iostream>
#include <chrono>
#include <numeric>
#include <cassert>
#include "cnf.h"
#include "weighted_problem.h"
#include "solution.h"
#include "reference_solution.h"
#include "algorithms/simulated_cooling/simulated_cooling.h"

namespace sat {
	void test_simulated_cooling(const std::vector<std::pair<std::string, weighted_problem>>& problems,
								const std::map<std::string, std::vector<weighted_problem::reference_solution>>& ref_solutions,
								const simulated_cooling& solver) {
		std::vector<std::chrono::microseconds::rep> time_measurements;
		std::vector<double> precision_measurements;
		for (const auto&[id, problem] : problems) {
			std::cout << id << std::endl;
			auto begin_time = std::chrono::system_clock::now();
			auto solution = solver.solve(problem);
			auto end_time = std::chrono::system_clock::now();
			std::chrono::microseconds::rep duration_in_microseconds = (end_time - begin_time).count();
			time_measurements.push_back(duration_in_microseconds);
			if (solution.has_value()) {
				if (auto it = ref_solutions.find(id); it == ref_solutions.end()) {
					std::cout << "Couldn't find reference solution for id " << id << std::endl;
				} else {
					weight_t expected_weight = it->second.front().weight();
					if (expected_weight < solution->weight()) {
						std::cerr << "Something is wrong!" << std::endl;
						assert(false);
					}
					double precision = 1;
					if (expected_weight != 0) {
						precision = (double) solution->weight() / expected_weight;
					}
					precision_measurements.push_back(precision);
					std::cout << precision << std::endl;
				}
			} else {
				if (auto it = ref_solutions.find(id); it != ref_solutions.end()) {
					std::cerr << "Solutions for " << id << " exists, but none has been found" << std::endl;
					precision_measurements.push_back(0);
				} else {
					std::cout << "We don't know anything about this" << std::endl;
				}
			}
		}
		double precision_sum = std::accumulate(precision_measurements.begin(), precision_measurements.end(), 0.0);
		std::chrono::microseconds::rep elapsed_time_sum = std::accumulate(time_measurements.begin(),
																		  time_measurements.end(),
																		  std::chrono::microseconds::zero().count());
		double precision_average = precision_sum / precision_measurements.size();
		auto elapsed_time_average = elapsed_time_sum / time_measurements.size();
		std::cout << "Average precision " << precision_average << " from " << precision_measurements.size() << " solutions\n" << "Average elapsed time "
				  << elapsed_time_average << std::endl;
	}
}