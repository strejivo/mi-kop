#ifndef MI_KOP_SHARED_RANDOM_H
#define MI_KOP_SHARED_RANDOM_H

#include <random>


double acceptance_percentage();

std::default_random_engine& random_engine();

#endif //MI_KOP_SHARED_RANDOM_H
