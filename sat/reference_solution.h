#ifndef REFERENCE_SOLUTION_H
#define REFERENCE_SOLUTION_H

#include "weighted_problem.h"

namespace sat {
	class weighted_problem::reference_solution {
	  public:
		reference_solution(evaluation_t v, const weight_t w) : m_values(std::move(v)), m_weight(w) {}

		const evaluation_t& values() const {
			return m_values;
		}

		weight_t weight() const {
			return m_weight;
		}

	  private:
		evaluation_t m_values;
		weight_t m_weight;
	};
}


#endif //REFERENCE_SOLUTION_H
