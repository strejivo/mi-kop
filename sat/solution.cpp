#include <cassert>
#include "solution.h"

namespace sat {
	weighted_problem::solution& weighted_problem::solution::operator=(const weighted_problem::solution& o) {
		if (&o != this) {
			assert(&m_problem == &o.m_problem);
			m_weight = o.m_weight;
			m_values = o.m_values;
		}
		return *this;
	}

	bool weighted_problem::solution::operator>(const weighted_problem::solution& o) const {
		assert(&m_problem == &o.m_problem);
		return m_weight > o.m_weight;
	}
}