#ifndef SOLUTION_H
#define SOLUTION_H

#include "weighted_problem.h"

namespace sat {
	class weighted_problem::solution {
	  public:
		solution(evaluation_t v, weight_t w, const weighted_problem& problem)
		  : m_values(std::move(v)),
			m_weight(w),
			m_problem(problem) {}

		solution(const solution&) = default;

		solution& operator=(const solution& o);

		bool operator>(const solution& o) const;

		const evaluation_t& values() const {
			return m_values;
		}

		weight_t weight() const {
			return m_weight;
		}

		const weighted_problem& problem() const {
			return m_problem;
		}

	  protected:
		evaluation_t m_values;
		weight_t m_weight;
		const weighted_problem& m_problem;
	};
}

#endif //SOLUTION_H
