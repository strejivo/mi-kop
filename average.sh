#!/bin/bash

INPUT_FOLDER=measurements/myinstances
OUTPUT_FOLDER=measurements/myinstances

average () {
  cat "$CURRENT_FILE" | awk '{ sum += $1 } END { if (NR > 0) printf "%f\n", sum / NR }' >> "$OUTPUT_FILE"
}

for solver in bnb price capacity greedy; do
  for price_weight_corr in uni corr strong; do
    CURRENT_FILE="$INPUT_FOLDER"/"$solver"_PRICE_"$price_weight_corr"
    OUTPUT_FILE="$OUTPUT_FOLDER"/Average"$solver"Price
    average
  done
  for bag_capacity_ratio in 0.1 0.3 0.5 0.8 1 2; do
    CURRENT_FILE="$INPUT_FOLDER"/"$solver"_BAG_RATIO_"$bag_capacity_ratio"
    OUTPUT_FILE="$OUTPUT_FOLDER"/Average"$solver"BagRatio
    average
  done
  for weight_distribution in bal light heavy; do
    if [ "$weight_distribution" != bal ]; then
      for k in 0.1 0.2 0.5 1 3 7; do
        CURRENT_FILE="$OUTPUT_FOLDER"/"$solver"_WEIGHT_DIST_"$weight_distribution"_"$k"
        OUTPUT_FILE="$OUTPUT_FOLDER"/Average"$solver"WeightDist"$weight_distribution"
        average
      done
    else
        CURRENT_FILE="$OUTPUT_FOLDER"/"$solver"_WEIGHT_DIST_"$weight_distribution"
        OUTPUT_FILE="$OUTPUT_FOLDER"/Average"$solver"WeightDistbal
        average
    fi;
  done
done


