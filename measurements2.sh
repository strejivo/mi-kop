#!/bin/bash

# Loop over all instance sizes
for i in 4 10 15 20 22 25 27 30 32 35 37 40; do
  # Loop over all solver algithms
  for type in capacity price fptas greedy redux; do
    # Loop over all data sets
    for ds in ZKC ZKW NK; do
      # Create compute process
     ./cmake-build-release/mi_kop "$type" data/"$ds""$i"_inst.dat data/"$ds""$i"_sol.dat >measurements/"$type"_"$ds"_"$i"_time 2>measurements/"$type"_"$ds"_"$i"_result
    done
  done
done
# Now lets wait for all of them to finish
wait
